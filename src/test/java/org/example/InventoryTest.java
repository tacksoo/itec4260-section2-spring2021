package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class InventoryTest {

    @Test
    public void testEmptyInventory() {
        Inventory inv = new Inventory();
        Assert.assertEquals(0, inv.getSize());
    }

    @Test
    public void testAddInventory() {
        Inventory inv = new Inventory();
        inv.add(new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 100));
        Assert.assertEquals(1, inv.getSize());
    }

    @Test
    public void testRemoveInventory() {
        Game g = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 100);
        Game g2 = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 100);
        Inventory inv = new Inventory();
        inv.add(g);
        inv.remove(g);
        Assert.assertEquals(0, inv.getSize());
        // check if remove() works with duplicate objects
        Inventory inv2 = new Inventory();
        inv.add(g);
        inv.add(g);
        inv.remove(g);
        Assert.assertEquals(1, inv.getSize());
    }

    @Test
    public void testCheapestGame() {
        Game g = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 100);
        Game g2 = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "Linux")),
                "Starcraft Remastered", LocalDate.of(2018, 1, 1), "Blizzard",
                new ArrayList<String>(Arrays.asList("RTS")), new BigDecimal("9.99"), "PG", 100);
        ;
        Inventory inv = new Inventory();
        inv.add(g);
        inv.add(g2);
        Assert.assertEquals(2, inv.getSize());
        Game cheapest = inv.getCheapestGame();
        Assert.assertNotNull(cheapest);
        Assert.assertEquals(new BigDecimal("6.99"), cheapest.getPrice());
    }

    @Test
    public void testGameScore() {
        Game g = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 98);
        Game g2 = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "Linux")),
                "Starcraft Remastered", LocalDate.of(2018, 1, 1), "Blizzard",
                new ArrayList<String>(Arrays.asList("RTS")), new BigDecimal("9.99"), "PG", 100);
        ;
        Inventory inv = new Inventory();
        inv.add(g);
        inv.add(g2);
        Assert.assertTrue(inv.getMostHighlyRatedGame().equals(g2));
    }

    @Test
    public void testAveragePrice() {
        Game g = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("10"), "PG", 98);
        Game g2 = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "Linux")),
                "Starcraft Remastered", LocalDate.of(2018, 1, 1), "Blizzard",
                new ArrayList<String>(Arrays.asList("RTS")), new BigDecimal("20"), "PG", 100);
        ;
        Inventory inv = new Inventory();
        inv.add(g);
        inv.add(g2);
        BigDecimal average = inv.getAveragePriceOfAllGames();
        Assert.assertEquals(new BigDecimal("15"),average);
    }


}
