package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Tests for CraigsList used car page Atlanta
 */
public class CarListingTest {

    private static WebDriver driver;
    private static Connection connection;
    private final static String DB_URL = "jdbc:sqlite:cars.db";
    private String CRAIGSLIST_URL = "https://atlanta.craigslist.org/d/cars-trucks/search/cta";

    @BeforeClass
    public static void setUpDriver() throws SQLException {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        connection = DriverManager.getConnection(DB_URL);
    }

    @Test
    public void testCarTableSize() throws SQLException {
        Assert.assertTrue(getCarTableSize() >= 0);
    }

    public int getCarTableSize() {
        String sql = "select count(*) from cars";
        int total = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            rs.next();
            total = rs.getInt("count(*)");
            ps.close();
            rs.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
        return total;
    }

    @Test
    public void testFrontPageListing() {
        driver.get(CRAIGSLIST_URL);
        int startNum = getCarTableSize();
        // make sure that there are 120 cars listed on front page
        List<WebElement> carRows = driver.findElements(By.className("result-row"));
        Assert.assertEquals(120, carRows.size());
        List<String> titles = new ArrayList<>();
        List<Double> prices = new ArrayList<>();
        for (int i = 0; i < carRows.size(); i++) {
            WebElement currentCar = carRows.get(i);
            WebElement title = currentCar.findElement(By.xpath(".//a[@class = 'result-title hdrlnk']"));
            String carURL = title.getAttribute("href");
            String postTitle = title.getText();
            titles.add(postTitle);
            WebElement price = currentCar.findElement(By.xpath(".//span[@class = 'result-price']"));
            prices.add(Double.parseDouble(price.getText().replace("$", "").replace(",", "")));
            double priceNum = Double.parseDouble(price.getText().replace("$", "").replace(",", ""));
            // print out the relevant info
            // subject line, price, URL and time of retrieval
            System.out.println(postTitle);
            System.out.println(priceNum);
            System.out.println(carURL);
            System.out.println(new Date().toString());
            addCarToDB(postTitle, (int) priceNum, carURL, new Date().toString());
        }
        int endNum = getCarTableSize();

        Assert.assertTrue( endNum - startNum == 120);
    }

    public void addCarToDB(String postTitle, int price, String url, String timestamp) {
        String sql = "insert into cars (title, price, url, timestamp) values (?, ? , ?, ?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, postTitle);
            ps.setInt(2, price);
            ps.setString(3, url);
            ps.setString(4, timestamp);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testJSONSerialize() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Car tesla = new Car("Used Tesla 2020", 30000, "https://cragislist.com", "3/24/2021");
        String json = mapper.writeValueAsString(tesla);
        System.out.println(json);
    }

    /**
     * print cheapest car given list of titles and prices, ignore $0 car
     * but scam artists will list monthly payment :)
     *
     * @param titles
     * @param prices
     */
    public void getCheapestCar(List<String> titles, List<Double> prices) {
        double cheapest = Double.MAX_VALUE;
        int cheapestIndex = 0;
        for (int i = 0; i < prices.size(); i++) {
            if (prices.get(i) == 0) continue;
            if (cheapest > prices.get(i)) {
                cheapest = prices.get(i);
                cheapestIndex = i;
            }
        }
        System.out.println("The cheapest car on front page is: \n" +
                titles.get(cheapestIndex) + "\n" + cheapest);
    }


}
