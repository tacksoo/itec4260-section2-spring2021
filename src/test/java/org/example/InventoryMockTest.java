package org.example;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class InventoryMockTest {

    @Test
    public void testCheapestMock() {
        InventoryInterface inventory = Mockito.mock(InventoryInterface.class);
        Game nfl = new Game(new ArrayList<String>(Arrays.asList("PC")),
                "Madden NFL 2021", LocalDate.of(2021, 1, 1), "EA",
                new ArrayList<String>(Arrays.asList("Sports")),
                new BigDecimal("60"), "E", 0);
        Mockito.when(inventory.findCheapestGame()).thenReturn(nfl);
        Assert.assertEquals("Madden NFL 2021", inventory.findCheapestGame().getName());
    }

    @Test
    public void testMostExpensiveMock() {
        InventoryInterface inventory = Mockito.mock(InventoryInterface.class);
        Game mario = new Game(new ArrayList<String>(Arrays.asList("Console")),
                "Super Mario Bros", LocalDate.of(1985, 1, 1), "Nintendo",
                new ArrayList<String>(Arrays.asList("Platformer")),
                new BigDecimal("660000"), "E", 100);
        Mockito.when(inventory.findMostExpensiveGame()).thenReturn(mario);
        Assert.assertEquals("Super Mario Bros",inventory.findMostExpensiveGame().getName());
    }




}
