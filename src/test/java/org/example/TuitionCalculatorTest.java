package org.example;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class TuitionCalculatorTest {

    private static WebDriver driver;
    private String CALC_URL = "http://www.ggc.edu/admissions/tuition-and-financial-aid-calculators/index.html#";
    private String US_NEWS = "https://www.usnews.com/best-colleges/georgia-gwinnett-college-41429";
    private String BANNER = "https://ggc.gabest.usg.edu/pls/B400/twbkwbis.P_WWWLogin";
    @BeforeClass
    public static void setUpSelenium() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void testOutOfStateTuition() {
        // skip this since easy :)
    }

    @Test
    public void testInStateTuition() {
        driver.get(CALC_URL);
        WebElement inStateRadioButton = driver.findElement(By.cssSelector("#main-content > div > article > div > div:nth-child(4) > form > div:nth-child(1) > div > div > div:nth-child(1) > fieldset > div > label:nth-child(1)"));
        inStateRadioButton.click();
        Select creditHours = new Select(driver.findElement(By.id("creditHOURS")));
        creditHours.selectByValue("15");
        WebElement priceElement = driver.findElement(By.cssSelector("#totalcost"));
        //System.out.println("Price " + priceElement.getAttribute("value"));
        double semesterTuition = Double.parseDouble(
                priceElement.getAttribute("value").trim().substring(1));
        //System.out.println(semesterTuition);
        driver.get(US_NEWS);
        WebElement usnewsPrice = driver.findElement(By.cssSelector("#app > div > div:nth-child(1) > div.Heading__ProfileHeadingBox-sc-6a32fq-5.kmKRSP > div > div > div > div.Villain__FlexDiv-sc-1y12ps5-0.duSOMV > div.Villain__SupplementColumn-sc-1y12ps5-2.bjOYsD > div > div > div > div.Panel__Content-ejd83f-0.bhdSjA.border-right.border-left.border-bottom > table > tbody > tr:nth-child(1) > div > a.Anchor-byh49a-0.esWvXN"));
        //System.out.println(usnewsPrice.getText());
        String yearlyString = usnewsPrice.getText().split(" ")[0].replace(",","").replace("$","");
        double yearlyPrice = Double.parseDouble(yearlyString);
        System.out.println(yearlyPrice);
        Assert.assertEquals(yearlyPrice, semesterTuition * 2, 10);
    }

    @Test
    public void testBannerTuition() {
        driver.get(BANNER);
        WebElement inputID = driver.findElement(By.cssSelector("#UserID"));
        inputID.sendKeys("900012345");
        WebElement password = driver.findElement(By.cssSelector("#PIN"));
        password.sendKeys("1234567");
        WebElement submit = driver.findElement(By.cssSelector("body > div.pagebodydiv > form > p > input[type=submit]:nth-child(1)"));
        submit.click();
    }
}
