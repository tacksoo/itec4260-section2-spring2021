package org.example;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class SearchPersonTest {

    private static WebDriver driver;

    @BeforeClass
    public static void setupDriver() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    //@Parameters({"bill gates","warren buffett","khloe kardashian"})
    @FileParameters("students.txt")
    public void searchPerson(String name) {
        driver.get("https://www.google.com");
        WebElement queryBox = driver.findElement(By.name("q"));
        queryBox.clear();
        queryBox.sendKeys(name);
        queryBox.submit();
        WebElement urlElement = driver.findElement(By.cssSelector("#rso > div > div:nth-child(1) > div > div.yuRUbf > a"));
        String url = urlElement.getAttribute("href");
        List<WebElement> headings = driver.findElements(By.tagName("h3"));
        String description = headings.get(0).getText();
        System.out.println(name + " " + description);
    }

}
