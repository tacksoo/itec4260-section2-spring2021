package org.example;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.Validate;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExceptionTest {

    @Test(expected = ArithmeticException.class)
    public void testDivideByZero() {
        int x = 10/0;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyList() {
        List<String> names = new ArrayList<>();
        Validate.notEmpty(names);
    }

    @Test
    public void testValidate() {
        boolean x = true;
        Validate.isTrue(x);
        String name = "bob";
        Validate.notNull(name);
    }

}
