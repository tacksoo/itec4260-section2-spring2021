package org.example;

public class Car {
    private String title;
    private double price;
    private String URL;
    private String postDate;

    public Car() {
    }

    public Car(String title, double price, String URL, String postDate) {
        this.title = title;
        this.price = price;
        this.URL = URL;
        this.postDate = postDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }
}
