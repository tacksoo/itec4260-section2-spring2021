package org.example;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.PriorityQueue;

@RunWith(JUnitParamsRunner.class)
public class SocialSecurityTest {

    private static WebDriver driver;
    private static String SSA_CALC = "https://www.ssa.gov/OACT/quickcalc/";

    @BeforeClass
    public static void setupDriver() {
        driver = new ChromeDriver();
        System.setProperty("webdriver.chrome.driver", "chromedriver");
    }

    @Test
    @Parameters({"01,01,1990,55000,2044",
            "01,01,1980,65000,2255", "01,01,1970,75000,2357"})
    public static void testPayout(int month, int day, int year, int salary, int payout) {
        driver.get(SSA_CALC);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys(month + "");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys(day + "");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys(year + "");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        earningsElement.sendKeys(salary+"");
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.submit();
        WebElement fraElement = driver.findElement(By.id("est_fra"));
        String fraString = fraElement.getText().replace("$","").replace(",","");
        int fraEstimate = (int) Double.parseDouble(fraString);
        Assert.assertEquals(payout,fraEstimate,60);
    }

    @Test
    @FileParameters("salary.csv")
    public void testMaxPayOut(int year, int salary) {
        driver.get(SSA_CALC);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys("01");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys("01");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys(year + "");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        earningsElement.sendKeys(salary+"");
        WebElement submit = driver.findElement(By.xpath("/html/body/table[4]/tbody/tr[2]/td[2]/form/table/tbody/tr[5]/td/input"));
        submit.submit();
        WebElement fraElement = driver.findElement(By.id("est_fra"));
        String fraString = fraElement.getText().replace("$","").replace(",","");
        int fraEstimate = (int) Double.parseDouble(fraString);
        System.out.println(year + " " + salary + " FRA $" + fraEstimate);
        WebElement lateElement = driver.findElement(By.id("est_late"));
        String lateString = lateElement.getText().replace("$","").replace(",","");
        int lateEstimate = (int) Double.parseDouble(lateString);
        System.out.println(year + " " + salary + " LATE $" + lateEstimate);
    }

    @Test
    public void testMiniRetirement() {
        driver.get(SSA_CALC);
        WebElement monthElement = driver.findElement(By.name("dobmon"));
        monthElement.clear();
        monthElement.sendKeys("01");
        WebElement dayElement = driver.findElement(By.name("dobday"));
        dayElement.clear();
        dayElement.sendKeys("01");
        WebElement yearElement = driver.findElement(By.name("yob"));
        yearElement.clear();
        yearElement.sendKeys("1970");
        WebElement earningsElement = driver.findElement(By.name("earnings"));
        earningsElement.clear();
        //earningsElement.sendKeys(salary+"");
    }
}
