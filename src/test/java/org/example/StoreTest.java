package org.example;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class StoreTest {

    private Store gamestop;

    @Before
    public void setUp() {
        gamestop = new Store();
        gamestop.loadInventoryFromWeb("https://pastebin.com/raw/UnZQxLJL");
    }

    @Test
    public void testLoadInventory() {
        Assert.assertEquals(6, gamestop.getInventory().getSize());
    }



    @Test
    public void testCSVToGames() throws IOException {
        URL url = new URL("https://pastebin.com/raw/UnZQxLJL");
        InputStream in = url.openStream();
        String csv = IOUtils.toString(in, "UTF-8");
        String[] lines = csv.split("\n");
        Assert.assertEquals(6, lines.length);

        for (int i = 0; i < lines.length; i++) {
            Game g = Store.getGameFromLine(lines[i]);
            g.printGame();
        }
    }

    @Test
    public void testCheapestGame() {
        // from https://pastebin.com/raw/UnZQxLJL
        Game cheapest = gamestop.getInventory().getCheapestGame();
        Assert.assertEquals("Pokemon Crystal",cheapest.getName());
    }

    @Test
    public void testMostExpensiveGame() {
        Game mostExpensive = gamestop.getInventory().getMostExpensiveGame();
        Assert.assertEquals("Animal Crossing",mostExpensive.getName());
    }

    @Test
    public void testAveragePriceOfGames() {

    }
}
