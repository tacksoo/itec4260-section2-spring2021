package org.example;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.List;

public class LinkedInTest {

    private static WebDriver driver;
    private static Connection connection;
    private final static String DB_URL = "jdbc:sqlite:people.db";

    @BeforeClass
    public static void setUp() throws SQLException {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        driver = new ChromeDriver();
        connection = DriverManager.getConnection(DB_URL);
    }

    @Test
    @Ignore
    public void testPeopleLinkedIn() throws IOException {
        List<String> names = FileUtils.readLines(new File("people.txt"),"UTF-8");
        for (int i = 0; i < names.size(); i++) {
            driver.get("https://www.google.com");
            WebElement searchBox = driver.findElement(By.name("q"));
            searchBox.sendKeys(names.get(i) + " ggc linkedin");
            searchBox.submit();
            WebElement urlElement = driver.findElement(By.cssSelector("#rso > div > div:nth-child(1) > div > div.yuRUbf > a"));
            String url = urlElement.getAttribute("href");
            List<WebElement> headings = driver.findElements(By.tagName("h3"));
            String description = headings.get(0).getText();
            System.out.println(names.get(i) + "\n" + url + "\n" + description + "\n");
            addPersonToDatabase(names.get(i), url, description);
        }
    }

    public void addPersonToDatabase(String name, String url, String description) {
        String sql = "insert into people (name, url, description) values (?, ?, ?);";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, name);
            ps.setString(2, url);
            ps.setString(3, description);
            ps.executeUpdate();
            ps.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPeopleTest() throws SQLException {
        String sql = "select * from people;";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();

        while(rs.next()) {
            System.out.println(rs.getString("url"));
        }
    }


    @AfterClass
    public static void cleanUp() {

    }
}
