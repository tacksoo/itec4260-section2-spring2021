package org.example;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class StonkTest {

    @Test
    public void testLowestLine() throws IOException {
        URL gistURL = new URL("https://gist.githubusercontent.com/tacksoo/b9edbfc8c03e1ca89d459bf1af39842d/raw/75abf553a0297d9202b1a568f185f735055d6f81/stonks.csv");
        String str = IOUtils.toString(gistURL.openStream(),"UTF-8");
        String[] lines = str.split("\n");
        String lowestLine = "";
        double lowestValue = Double.MAX_VALUE;
        for (int i = 1; i < lines.length; i++) {
            String[] rowElements = lines[i].split(",");
            int length = rowElements[2].trim().length();
            double current = Double.parseDouble(
                    rowElements[2].trim().substring(0,length-1));
            if (lowestValue > current) {
                lowestValue = current;
                lowestLine = lines[i];
            }
        }
        FileUtils.writeStringToFile(new File("stonk.csv"),lowestLine,"UTF-8");
    }
}
