package org.example;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDate;

public class DateTest {

    @Test
    public void testDate() {
        LocalDate startDate = LocalDate.of(2021, 5, 3);
        LocalDate endDate = startDate.plusDays(6);
        Assert.assertEquals(9, endDate.getDayOfMonth());
        LocalDate doomsday = startDate.plusDays(1000000);
        System.out.println(doomsday.getYear());
        System.out.println(doomsday.getDayOfMonth());
        System.out.println(doomsday.getMonth());
    }
}
