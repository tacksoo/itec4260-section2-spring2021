package org.example;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.List;

public class HelloSelenium {

    private static WebDriver driver;

    @BeforeClass
    public static void setUpChrome() {
        System.setProperty("webdriver.chrome.driver","chromedriver");
        //ChromeOptions options = new ChromeOptions();
        //options.addArguments("headless");
        driver = new ChromeDriver();
    }

    @Test
    @Ignore
    public void testGoogle() {
        driver.get("https://www.google.com");
        WebElement queryBox = driver.findElement(By.name("q"));
        queryBox.clear();
        queryBox.sendKeys("GGC");
        queryBox.submit();
        Assert.assertTrue(driver.getTitle().contains("GGC"));
    }

    @Test
    public void testJobDescriptionGoogle() {
        driver.get("https://www.google.com");
        WebElement queryBox = driver.findElement(By.name("q"));
        queryBox.sendKeys("zachary idrees linkedin");
        queryBox.submit();
        List<WebElement> headings = driver.findElements(By.tagName("h3"));
        for (int i = 0; i < headings.size(); i++) {
            System.out.println(headings.get(i).getText());
        }
    }

    @AfterClass
    public static void cleanUp() throws Exception {
        Thread.sleep(5000);
        driver.close();
    }
}
