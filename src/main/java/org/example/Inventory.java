package org.example;

import org.apache.commons.lang3.Validate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * This class keeps track of an inventory of Games
 * Designed to maintain a list of games and find interesting properties of the games
 *
 *
 */
public class Inventory {
    private List<Game> games = new ArrayList<>();

    public Inventory() {
    }

    /**
     * Returns the size of the inventory
     * @return size of inventory, will always be non-negative
     */
    public int getSize() {
        return games.size();
    }

    /**
     * Add a {@code Game} object to the inventory. Duplicate Game objects are allowed
     *
     * @param game a non-null Game object
     */
    public void add(Game game) {
        Validate.notNull(game);
        games.add(game);
    }

    /**
     * Remove Game object from inventory, this will only remove the <b>first</b> instance of the Game object !
     *
     * @param g a Game object
     */
    public void remove(Game g) {
        games.remove(g);
    }

    /**
     * Gets the cheapest (in terms of price) game in the inventory.
     * If more than one game as the cheapest price, the LATEST game
     * that was added to the inventory with the cheapest price is returned.
     * Also, the inventory has to be NON-EMPTY!
     * @return cheapest game in terms of price
     */
    public Game getCheapestGame() {
        BigDecimal price = games.get(0).getPrice();
        Game cheapest = games.get(0);
        for (Game g: games) {
            if( cheapest.getPrice().compareTo( g.getPrice() ) == 1) {
                cheapest = g;
            }
        }
        return cheapest;
    }

    /**
     *  Gets the most expensive (in terms of price) game in the inventory.
     *  If more than one game as the most expensive price, the LATEST game
     *  that was added to the inventory with the most expensive price is returned.
     *  Also, the inventory has to be NON-EMPTY!
     *  @return most expensive game in terms of price
     */
    public Game getMostExpensiveGame() {
        BigDecimal price = games.get(0).getPrice();
        Game mostExpensive = games.get(0);
        for (Game g: games) {
            if( mostExpensive.getPrice().compareTo( g.getPrice() ) == -1) {
                mostExpensive = g;
            }
        }
        return mostExpensive;
    }

    /**
     * Gets the game with the highest "score". We assume the inventory to be non-empty.
     * If one than one game has the highest score, then the game that was added latest
     * (with the highest score) is returned
     *
     * @return Game with the highest score
     */
    public Game getMostHighlyRatedGame() {
        Game highest = games.get(0);
        for(Game g: games) {
            if(highest.getScore() <= g.getScore()) {
                highest = g;
            }
        }
        return highest;
    }

    /**
     * Returns the average price of all games in inventory
     * @return average price of games in inventory
     */
    public BigDecimal getAveragePriceOfAllGames() {
        BigDecimal total = new BigDecimal("0");
        for(Game g: games) {
            total = total.add(g.getPrice());
        }
        return total.divide(new BigDecimal(games.size() + ""));
    }

    /**
     * Print to console the average price of all games in inventory
     */
    public void printAveragePriceOfAllGames() {
        System.out.println(getAveragePriceOfAllGames());
    }
}
