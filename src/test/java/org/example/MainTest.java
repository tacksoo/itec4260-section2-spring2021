package org.example;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

public class MainTest {

    @Test
    public void printGames() {
        ArrayList<Game> drimGames = new ArrayList<Game>();

        Game woods = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "Night in the Woods", LocalDate.of(2017, 1, 10), "Infinite Fall",
                new ArrayList<String>(Arrays.asList("Adventure", "Platformer")),
                new BigDecimal("6.99"), "PG", 100);
        drimGames.add(woods);

        Game dark = new Game(new ArrayList<String>(Arrays.asList("PC", "Mac")),
                "The Long Dark", LocalDate.of(2014, 9, 22), "Hinterland Studio",
                new ArrayList<String>(Arrays.asList("Survival", "3d")),
                new BigDecimal("29.99"), "PG", 99);

        Game starcraft = new Game(new ArrayList<>(Arrays.asList("PC", "Mac", "Linux")),
                "Starcraft Remastered", LocalDate.of(2018, 1, 1), "Blizzard",
                new ArrayList<String>(Arrays.asList("RTS")), new BigDecimal("9.99"), "PG", 100);

        drimGames.add(dark);
        drimGames.add(starcraft);

        for (int i = 0; i < drimGames.size(); i++) {
            drimGames.get(i).printGame();
        }
    }

}
